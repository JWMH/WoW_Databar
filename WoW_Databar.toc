## Interface: 70000
## Title: WoW_Databar
## Author: Cow_Fenris
## Notes: A Fork of SX_Databar by saxitoxin
## SavedVariables: TEST_CONFIG

settings.lua

modules\config.lua
modules\social.lua
modules\micromenu.lua
modules\armor.lua
modules\talent.lua
modules\clock.lua
modules\tradeskill.lua
modules\currency.lua
modules\performance.lua
modules\system.lua
modules\gold.lua
modules\heartstone.lua
