local addon, ns = ...
local cfg = ns.cfg
local unpack = unpack
--------------------------------------------------------------
if not cfg.currency.show then return end

local currencyFrame = CreateFrame("Frame",nil, cfg.SXframe)
currencyFrame:SetPoint("LEFT", cfg.SXframe, "CENTER", 340,0)
currencyFrame:SetSize(16, 16)

---------------------------------------------
-- XP BAR
---------------------------------------------
local xpFrame = CreateFrame("BUTTON",nil, cfg.SXframe)
xpFrame:SetPoint("LEFT", cfg.SXframe, "CENTER", 350,0)
xpFrame:SetSize(16, 16)
xpFrame:EnableMouse(true)
xpFrame:RegisterForClicks("AnyUp")

local xpIcon = xpFrame:CreateTexture(nil,"OVERLAY",nil,7)
xpIcon:SetSize(16, 16)
xpIcon:SetPoint("LEFT")
xpIcon:SetTexture(cfg.mediaFolder.."datatexts\\exp")
xpIcon:SetVertexColor(unpack(cfg.color.normal))

local xpText = xpFrame:CreateFontString(nil, "OVERLAY")
xpText:SetFont(cfg.text.font, cfg.text.normalFontSize)
xpText:SetPoint("RIGHT",xpFrame,2,0 )
xpText:SetTextColor(unpack(cfg.color.normal))

local xpStatusbar = CreateFrame("StatusBar", nil, xpFrame)
xpStatusbar:SetStatusBarTexture(1,1,1)
xpStatusbar:SetStatusBarColor(unpack(cfg.color.normal))
xpStatusbar:SetPoint("TOPLEFT", xpText, "BOTTOMLEFT",0,-2)

local xpStatusbarBG = xpStatusbar:CreateTexture(nil,"BACKGROUND",nil,7)
xpStatusbarBG:SetPoint("TOPLEFT", xpText, "BOTTOMLEFT",0,-2)
xpStatusbarBG:SetTexture(unpack(cfg.color.inactive))

xpFrame:SetScript("OnEnter", function()
	if InCombatLockdown() then return end
	xpIcon:SetVertexColor(unpack(cfg.color.hover))
	xpStatusbar:SetStatusBarColor(unpack(cfg.color.hover))
	if not cfg.currency.showTooltip then return end
	local mxp = UnitXPMax("player")
	local xp = UnitXP("player")
	local nxp = mxp - xp
	local rxp = GetXPExhaustion()
	local name, standing, minrep, maxrep, value = GetWatchedFactionInfo()

	if cfg.core.position ~= "BOTTOM" then
		GameTooltip:SetOwner(xpStatusbar, cfg.tooltipPos)
	else
		GameTooltip:SetOwner(xpFrame, cfg.tooltipPos)
	end

	GameTooltip:AddLine("[|cff6699FFExperience Bar|r]")
	GameTooltip:AddLine(" ")
	GameTooltip:AddDoubleLine(COMBAT_XP_GAIN, format(cfg.SVal(xp)).."|cffffd100/|r"..format(cfg.SVal(mxp)).." |cffffd100/|r "..floor((xp/mxp)*1000)/10 .."%",NORMAL_FONT_COLOR.r,NORMAL_FONT_COLOR.g,NORMAL_FONT_COLOR.b,1,1,1)
	GameTooltip:AddDoubleLine(NEED, format(cfg.SVal(nxp)).." |cffffd100/|r "..floor((nxp/mxp)*1000)/10 .."%",NORMAL_FONT_COLOR.r,NORMAL_FONT_COLOR.g,NORMAL_FONT_COLOR.b,1,1,1)
	if rxp then
		GameTooltip:AddDoubleLine(TUTORIAL_TITLE26, format(cfg.SVal(rxp)) .." |cffffd100/|r ".. floor((rxp/mxp)*1000)/10 .."%", NORMAL_FONT_COLOR.r,NORMAL_FONT_COLOR.g,NORMAL_FONT_COLOR.b,1,1,1)
	end
	GameTooltip:Show()
end)

xpFrame:SetScript("OnLeave", function()
	xpIcon:SetVertexColor(unpack(cfg.color.normal))
	xpStatusbar:SetStatusBarColor(unpack(cfg.color.normal))
	if ( GameTooltip:IsShown() ) then GameTooltip:Hide() end
end)

---------------------------------------------
-- REROLL
---------------------------------------------
local rerollFrame = CreateFrame("BUTTON",nil, currencyFrame)
rerollFrame:SetPoint("LEFT")
rerollFrame:SetSize(16, 16)
rerollFrame:EnableMouse(true)
rerollFrame:RegisterForClicks("AnyUp")

local rerollIcon = rerollFrame:CreateTexture(nil,"OVERLAY",nil,7)
rerollIcon:SetSize(16, 16)
rerollIcon:SetPoint("RIGHT")
rerollIcon:SetTexture(cfg.mediaFolder.."datatexts\\reroll")
rerollIcon:SetVertexColor(unpack(cfg.color.inactive))

local rerollText = rerollFrame:CreateFontString(nil, "OVERLAY")
rerollText:SetFont(cfg.text.font, cfg.text.normalFontSize)
rerollText:SetPoint("RIGHT",rerollIcon,"LEFT",-2,0)
rerollText:SetTextColor(unpack(cfg.color.inactive))

rerollFrame:SetScript("OnEnter", function()
	if InCombatLockdown() then return end
	rerollIcon:SetVertexColor(unpack(cfg.color.hover))
	if not cfg.currency.showTooltip then return end
	GameTooltip:SetOwner(currencyFrame, cfg.tooltipPos)
	GameTooltip:AddLine("[|cff6699FFReroll|r]")
	GameTooltip:AddLine(" ")
	local SoBFFname, SoBFFamount, SoBFFicon, SoBFFearnedThisWeek, SoBFFweeklyMax, SoBFFtotalMax, SoBFFisDiscovered = GetCurrencyInfo(1273)
	if SoBFFamount > 0 then
		GameTooltip:AddLine(SoBFFname,1,1,0)
		GameTooltip:AddDoubleLine("|cffffff00Weekly: |cffffffff"..SoBFFearnedThisWeek.."|cffffff00/|cffffffff"..SoBFFweeklyMax, "|cffffff00Total: |cffffffff"..SoBFFamount.."|cffffff00/|cffffffff"..SoBFFtotalMax)
	end
	GameTooltip:Show()
end)

rerollFrame:SetScript("OnLeave", function()
	if ( GameTooltip:IsShown() ) then GameTooltip:Hide() end
	rerollIcon:SetVertexColor(unpack(cfg.color.inactive))
end)

rerollFrame:SetScript("OnClick", function(self, button, down)
	if InCombatLockdown() then return end
	if button == "LeftButton" then
		ToggleCharacter("TokenFrame")
	end
end)



---------------------------------------------
-- OrderHall RECOURCES
---------------------------------------------

local OrderHallFrame = CreateFrame("BUTTON",nil, currencyFrame)
OrderHallFrame:SetPoint("RIGHT",2,0)
OrderHallFrame:SetSize(16, 16)
OrderHallFrame:EnableMouse(true)
OrderHallFrame:RegisterForClicks("AnyUp")

local OrderHallIcon = OrderHallFrame:CreateTexture(nil,"OVERLAY",nil,7)
OrderHallIcon:SetSize(16, 16)
OrderHallIcon:SetPoint("RIGHT")
OrderHallIcon:SetTexture(cfg.mediaFolder.."datatexts\\garres")
OrderHallIcon:SetVertexColor(unpack(cfg.color.inactive))

local OrderHallText = OrderHallFrame:CreateFontString(nil, "OVERLAY")
OrderHallText:SetFont(cfg.text.font, cfg.text.normalFontSize)
OrderHallText:SetPoint("LEFT")
OrderHallText:SetTextColor(unpack(cfg.color.inactive))

OrderHallFrame:SetScript("OnEnter", function()
	if InCombatLockdown() then return end
	OrderHallIcon:SetVertexColor(unpack(cfg.color.hover))
	if not cfg.currency.showTooltip then return end
	GameTooltip:SetOwner(currencyFrame, cfg.tooltipPos)
	GameTooltip:AddLine("[|cff6699FFOrder Hall Recources|r]")
	GameTooltip:AddLine(" ")
	local ohName, ohAmount, _, _, _, ohTotalMax = GetCurrencyInfo(1220)
	local manaName, manaAmount, _, _, _, manaTotalMax, manaIsDiscovered = GetCurrencyInfo(1155)

	GameTooltip:AddDoubleLine(ohName, "|cffffffff"..format(cfg.SVal(ohAmount)))
	if manaIsDiscovered then
		GameTooltip:AddDoubleLine(manaName, "|cffffffff"..format(cfg.SVal(manaAmount)).."|cffffff00/|cffffffff"..format(cfg.SVal(manaTotalMax)))
	end
	GameTooltip:Show()
end)

OrderHallFrame:SetScript("OnLeave", function()
	if ( GameTooltip:IsShown() ) then GameTooltip:Hide() end
	OrderHallIcon:SetVertexColor(unpack(cfg.color.inactive))
end)

OrderHallFrame:SetScript("OnClick", function(self, button, down)
	if InCombatLockdown() then return end
	if button == "LeftButton" then
		ToggleCharacter("TokenFrame")
	end
end)


---------------------------------------------
-- FUNCTIONS
---------------------------------------------
local function updateXP(xp, mxp)
	if UnitLevel("player") == MAX_PLAYER_LEVEL or not cfg.currency.showXPbar then
		xpFrame:Hide()
		xpFrame:EnableMouse(false)
		currencyFrame:Show()
	else
		currencyFrame:Hide()
		xpFrame:Show()
		xpFrame:EnableMouse(true)
		xpStatusbar:SetMinMaxValues(0, mxp)
		xpStatusbar:SetValue(xp)
		xpText:SetText("LEVEL "..UnitLevel("player").." "..cfg.CLASS)
		xpFrame:SetSize(xpText:GetStringWidth()+18, 16)
		xpStatusbar:SetSize(xpText:GetStringWidth(),3)
		xpStatusbarBG:SetSize(xpText:GetStringWidth(),3)
	end
end

---------------------------------------------
-- EVENT HANDELING
---------------------------------------------

local eventframe = CreateFrame("Frame")
eventframe:RegisterEvent("PLAYER_ENTERING_WORLD")
eventframe:RegisterEvent("PLAYER_XP_UPDATE")
eventframe:RegisterEvent("PLAYER_LEVEL_UP")
eventframe:RegisterEvent("CURRENCY_DISPLAY_UPDATE")
eventframe:RegisterEvent("CHAT_MSG_CURRENCY")
eventframe:RegisterEvent("TRADE_CURRENCY_CHANGED")
eventframe:RegisterEvent("MODIFIER_STATE_CHANGED")

eventframe:SetScript("OnEvent", function(this, event, arg1, arg2, arg3, arg4, ...)
	--if event == "PLAYER_ENTERING_WORLD" or event == "PLAYER_XP_UPDATE" or event == "PLAYER_LEVEL_UP" then
	if UnitLevel("player") ~= MAX_PLAYER_LEVEL and cfg.currency.showXPbar then
		mxp = UnitXPMax("player")
		xp = UnitXP("player")
		updateXP(xp, mxp)
		currencyFrame:Hide()
	else
		xpFrame:Hide()
	end

	if event == "MODIFIER_STATE_CHANGED" then
		if InCombatLockdown() then return end
		if arg1 == "LSHIFT" or arg1 == "RSHIFT" then
			if UnitLevel("player") == MAX_PLAYER_LEVEL or not cfg.currency.showXPbar then return end
			if arg2 == 1 then
				xpFrame:Hide()
				xpFrame:EnableMouse(false)
				currencyFrame:Show()
			elseif arg2 == 0 then
				currencyFrame:Hide()
				xpFrame:EnableMouse(true)
				xpFrame:Show()
			end
		end
	end




	-- reroll currency
	local SoBFFname, SoBFFamount, _, _, _, SoBFFtotalMax, SoBFFisDiscovered = GetCurrencyInfo(1273)
	if SoBFFamount > 0 then
			rerollText:SetText(SoBFFamount)
	else
			rerollFrame:Hide()
	end
	rerollFrame:SetSize(rerollText:GetStringWidth()+18, 16)

	-- OrderHall currency
	local ohName, ohAmount, _, ohEarnedThisWeek, ohWeeklyMax, ohTotalMax, ohIsDiscovered = GetCurrencyInfo(1220)
	OrderHallText:SetText(ohAmount)
	OrderHallFrame:SetSize(OrderHallText:GetStringWidth()+18, 16)

	currencyFrame:SetSize(rerollFrame:GetWidth()+OrderHallFrame:GetWidth()+6,16)
end)
